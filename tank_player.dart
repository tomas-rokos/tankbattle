part of tankbattle;

abstract class TankPlayer {
	BattleMap _map;
	int _myId;
	int get myId => _myId;
	void set myId(int val) {
		_myId = val;
	}
	BattleMap get map => _map;
	TankPlayer(this._map);
	void init();
	int doAction(); //1 - 9 move, 11-19 - shoot
}