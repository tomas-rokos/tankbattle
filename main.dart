library tankbattle;

import 'dart:html';
import 'dart:async';
import 'dart:math';

part "battle_map.dart";
part "tank_player.dart";
part "tank_tomas.dart";
part "tank_kamil.dart";

const int TIMING = 500000; // one "frame" in microseconds

final DivElement clocks = new DivElement();
final BattleMap battleMap = new BattleMap();

void main() {
	document.body.append(clocks);

	battleMap.registerPlayer(new TankTomas(battleMap), new Point<int>(3,3));
	battleMap.registerPlayer(new TankKamil(battleMap), new Point<int>(18,10));
	
	battleMap.render();
	battleMap.initPlayers();
	
    delta_timing();
}

void delta_timing() {
	var watch = new Stopwatch()..start();
	clocks.text = new DateTime.now().toIso8601String();

    bool cont = battleMap.proceed();
    if (!cont)
    	return;

    int microsecs = watch.elapsedMicroseconds;
    int newdelta = TIMING - microsecs;
    if (newdelta<1000)
    	newdelta = 1000;
    var timer = new Timer(new Duration(microseconds:newdelta), () {delta_timing(); });	
}