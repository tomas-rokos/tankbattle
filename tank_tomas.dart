part of tankbattle;

class TankTomas extends TankPlayer {
	int _rivalId;
	Random _rnd;
	TankTomas(BattleMap a) :super(a);
	void init() {
		_rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
		for (int idx = 0;idx<map.tankCount;++idx) {
			if (idx != myId)
				_rivalId = idx;
			
		}
	}
	bool lastShoot = false;
	int doAction()  {//1 - 9 move, 11-19 - shoot
		Point<int> myPos = map.positionOfTank(myId);
		Point<int> rivalPos = map.positionOfTank(_rivalId);
		Point<int> dir = rivalPos - myPos;
		if (dir.magnitude<4) {
			// panic
			dir = new Point<int>(-dir.x,-dir.y);
		} else if (dir.magnitude>10) {
		} else if (dir.magnitude>6) {
			if (_rnd.nextBool()) {
				switch (getDirection(dir)) {
					case 2:return _rnd.nextBool() ? 4 : 6; 
					case 8:return _rnd.nextBool() ? 4 : 6; 
					case 4:return _rnd.nextBool() ? 2 : 8; 
					case 6:return _rnd.nextBool() ? 2 : 8; 
				}
			}
		} else if (lastShoot == false) {
			lastShoot = true;
			return getDirection(dir)+10;
		} else {
			lastShoot = false;
		}
		return getDirection(dir);
	}
	int getDirection(Point<int> dir) {
		if (dir.x.abs() > dir.y.abs()) {
			return dir.x>0 ? 6 : 4;
		} else {
			return dir.y>0 ? 8 : 2;			
		}
		return 5;
		
	}
}