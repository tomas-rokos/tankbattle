part of tankbattle;

class TankKamil extends TankPlayer{
	int       _gunshot = 5;
	List<int> _rivals = new List<int>();

	TankKamil(BattleMap a) :super(a);
	
	void init() {
		for(int idx = 0; idx < map.tankCount; ++idx){
			if(idx != myId){
				_rivals.add(idx);
			}
		}
	}
	
	//1 - 9 move, 11-19 - shoot
	int doAction(){
		List<int> fireFieldRivals = this._getRivalsInFieldOfFire();
		// fire
		if(fireFieldRivals.length > 0){
			return this._getMissileDirection(fireFieldRivals);	
		}
		
		// move
		return _getDirectionTo(this._map.positionOfTank(this._getClosestFival()));
	}
	
	List<int> _getRivalsInFieldOfFire(){
		List<int> fireFieldRivals = new List<int>();
		
		Point<int> myPos = this._map.positionOfTank(this.myId);
		
		this._rivals.forEach((rivalIdx){
			Point<int> rivalPos = this._map.positionOfTank(rivalIdx);
			if(this._getDistanceBetween(myPos, rivalPos) < (this._gunshot + 1)){
				fireFieldRivals.add(rivalIdx);
			}
		});
		
		return fireFieldRivals;
	}
	
	int _getMissileDirection(List<int> targets){
		if(targets.length == 0){ return 11; }
		
		if(targets.length == 1){
			return this._getDirectionTo(this._map.positionOfTank(targets[0])) + 10;
		}
		
		// TEMP
		return this._getDirectionTo(this._map.positionOfTank(targets[0])) + 10;
		// TEMP
	}
	
	int _getDirectionTo(Point<int> target){
		Point<int> myPos = this._map.positionOfTank(this.myId);
		double degreeses = this._getAngleBetween(myPos, target);
		return this._getAngleToDirection(degreeses);
	}
	
	double _getAngleBetween(Point<int> point1, Point<int> point2){
		return atan2(point2.y - point1.y, point2.x - point1.x) * 180 / PI;
	}
	
	double _getDistanceBetween(Point<int> point1, Point<int> point2){
		double xPow = pow(point1.x.toDouble() - point2.x.toDouble(), 2);
		double yPow = pow(point1.y.toDouble() - point2.y.toDouble(), 2);
		
		return pow(xPow + yPow, 0.5);
	}
	
	int _getAngleToDirection(double degreeses){
		degreeses = degreeses % 360;
		if(degreeses < 0){ 
			degreeses += 360;
		}
		
		if(degreeses < (0 + 23))       { return 6; }
   	else if(degreeses < (45 + 23)) { return 3; }
   	else if(degreeses < (90 + 23)) { return 2; }
   	else if(degreeses < (135 + 23)){ return 1; }
   	else if(degreeses < (180 + 23)){ return 4; }
   	else if(degreeses < (225 + 23)){ return 7; }
   	else if(degreeses < (270 + 23)){ return 8; }
   	else if(degreeses < (315 + 23)){ return 9; }
   	else                           { return 6; }
	}
	
	int _getClosestFival(){
		Point<int> myPos = this._map.positionOfTank(this.myId);
		int clossestRivalId = -1;
		double clossestRivalDistance = 100000.0;
		
		this._rivals.forEach((rivalIdx){
      	Point<int> rivalPos = this._map.positionOfTank(rivalIdx);
      	
      	double distance = this._getDistanceBetween(myPos, rivalPos);
      	if(distance < clossestRivalDistance){
      		clossestRivalDistance = distance;
      		clossestRivalId = rivalIdx;	
      	}
      });
		
		if(clossestRivalId == -1){
			clossestRivalId = this._rivals[0];
		}
		
		return clossestRivalId;
	}
}