part of tankbattle;

class BattleMap {
	List<TankPlayer> _players = new List<TankPlayer>();
	List<Point<int>> _positions = new List<Point<int>>();
	List<String> _colors = ["red","blue","yellow"];
	
	int _currIdx = 0;
	
	int get width => 20;
	int get height => 20;
	
	BattleMap() {
		
	}
	
	void registerPlayer(TankPlayer pl, Point<int> pos) {
		int idx = _players.length;
		pl.myId = idx;
		_players.add(pl);
		_positions.add(pos);
	}
	
	Point<int> positionOfTank(int tankId) => _positions[tankId];
	int get tankCount => _players.length;
	List<List<TableCellElement>> _cells;
	
	void render() {
		TableElement table = new TableElement();
		table.id = "hracipole";
		document.body.append(table);
		_cells = new List<List<TableCellElement>>();
		for (int y = 0;y<height;++y) {
			_cells.add(new List<TableCellElement>());
			TableRowElement rowElm = new TableRowElement();
			for (int x = 0;x<width;++x) {
				TableCellElement cell = new TableCellElement();
				cell.innerHtml = " ";
				rowElm.append(cell);
				_cells[y].add(cell);
			}
			table.append(rowElm);
		}	
		renderScene();
	}
	void initPlayers() {
		_players.forEach((TankPlayer pl) {
			pl.init();
		});
	}
	
	bool proceed() {
		TankPlayer curr = _players[_currIdx];
		int curract = curr.doAction();
		int act = curract;
		Point<int> pos = _positions[_currIdx];
		Point<int> newPos;
		int incr = 1;
		if (act > 10) {
			act -= 10;
			incr = 5;
			
		}
		switch (act) {
			case 1: newPos = new Point<int>(pos.x-incr,pos.y-incr);break;
			case 2: newPos = new Point<int>(pos.x,pos.y-incr);break;
			case 3: newPos = new Point<int>(pos.x+incr,pos.y-incr);break;
			case 4: newPos = new Point<int>(pos.x-incr,pos.y);break;
			case 5: newPos = new Point<int>(pos.x,pos.y);break;
			case 6: newPos = new Point<int>(pos.x+incr,pos.y);break;
			case 7: newPos = new Point<int>(pos.x-incr,pos.y+incr);break;
			case 8: newPos = new Point<int>(pos.x,pos.y+incr);break;
			case 9: newPos = new Point<int>(pos.x+incr,pos.y+incr);break;
		}
		bool cont = true;
		if (curract > 10) {
			for (int xi = -1;xi<2;++xi) {
				for (int yi = -1;yi<2;++yi) {
					Point<int> sho = correctPos(new Point<int>(newPos.x+xi,newPos.y+yi));
					if (_cells[sho.y][sho.x].style.backgroundColor != "white" && _cells[sho.y][sho.x].style.backgroundColor != "black")
						cont = false;
					_cells[sho.y][sho.x].style.backgroundColor = "black";
    			}
			}
		} else {
			newPos = correctPos(newPos);
			_positions[_currIdx] = newPos;
			renderScene();
		}
		
		
		if (++_currIdx == _players.length)
			_currIdx = 0;
		
		return cont;
	}
	void renderScene() {
		_cells.forEach((List<TableCellElement> subl) {
			subl.forEach((TableCellElement elm) {
				elm.style.backgroundColor = "white";
			});
		});
		for (int idx = 0;idx<_positions.length;++idx) {
			Point<int> pos = _positions[idx];
			_cells[pos.y][pos.x].style.backgroundColor = _colors[idx];
		}
	}
	Point<int> correctPos(Point<int> pos) {
		Point<int> newPos = pos;
		if (newPos.x<0)
			newPos = new Point<int>(0,newPos.y);
		if (newPos.y<0)
			newPos = new Point<int>(newPos.x,0);
		if (newPos.x>=width)
			newPos = new Point<int>(width-1,newPos.y);
		if (newPos.y>=height)
			newPos = new Point<int>(newPos.x,height-1);
		return newPos;
	}
}